
#include <AudioToolbox/AudioToolbox.h>
#include "CAXException.h"
#include "CAStreamBasicDescription.h"
#include "CAAudioUnit.h"

AudioUnit matrixMixerAU;
CAAudioUnit matMixAU;

void UsageString(int exitCode)
{
	printf ("Usage: PlayFile /path/to/file\n");
	exit(exitCode);
}

// helper functions
double PrepareFileAU (CAAudioUnit &au, CAStreamBasicDescription &fileFormat, AudioFileID audioFile);
void MakeSimpleGraph (AUGraph &theGraph, CAAudioUnit &fileAU, CAStreamBasicDescription &fileFormat, AudioFileID audioFile);


int main (int argc, char * const argv[]) 
{
	if (argc < 2) UsageString(1);

	AudioFileID audioFile;
	
	const char* inputFile = argv[1];
    printf("file: %s\n", argv[1]);
	CFURLRef theURL = CFURLCreateFromFileSystemRepresentation(kCFAllocatorDefault, (UInt8*)inputFile, strlen(inputFile), false);
    XThrowIfError (!theURL, "CFURLCreateFromFileSystemRepresentation");
	
    OSStatus err = AudioFileOpenURL (theURL, kAudioFileReadPermission, 0, &audioFile);
    CFRelease(theURL);
	XThrowIfError (err, "AudioFileOpenURL");
			
			// get the number of channels of the file
	CAStreamBasicDescription fileFormat;
	UInt32 propsize = sizeof(CAStreamBasicDescription);
	XThrowIfError (AudioFileGetProperty(audioFile, kAudioFilePropertyDataFormat, &propsize, &fileFormat), "AudioFileGetProperty");
	
	printf ("playing file: %s\n", inputFile);
	printf ("format: "); fileFormat.Print();


// lets set up our playing state now
	AUGraph theGraph;
	CAAudioUnit fileAU;

// this makes the graph, the file AU and sets it all up for playing
	MakeSimpleGraph (theGraph, fileAU, fileFormat, audioFile);
		

// now we load the file contents up for playback before we start playing
// this has to be done the AU is initialized and anytime it is reset or uninitialized
	Float64 fileDuration = PrepareFileAU (fileAU, fileFormat, audioFile);
		printf ("file duration: %f secs\n", fileDuration);

// start playing
	XThrowIfError (AUGraphStart (theGraph), "AUGraphStart");
	
// sleep until the file is finished
	usleep ((int)(fileDuration * 1000. * 1000.));

// lets clean up
	XThrowIfError (AUGraphStop (theGraph), "AUGraphStop");
	XThrowIfError (AUGraphUninitialize (theGraph), "AUGraphUninitialize");
	XThrowIfError (AudioFileClose (audioFile), "AudioFileClose");
	XThrowIfError (AUGraphClose (theGraph), "AUGraphClose");
	
	return 0;
}	

double PrepareFileAU (CAAudioUnit &au, CAStreamBasicDescription &fileFormat, AudioFileID audioFile)
{	
// 
		// calculate the duration
	UInt64 nPackets;
	UInt32 propsize = sizeof(nPackets);
	XThrowIfError (AudioFileGetProperty(audioFile, kAudioFilePropertyAudioDataPacketCount, &propsize, &nPackets), "kAudioFilePropertyAudioDataPacketCount");
		
	Float64 fileDuration = (nPackets * fileFormat.mFramesPerPacket) / fileFormat.mSampleRate;

	ScheduledAudioFileRegion rgn;
	memset (&rgn.mTimeStamp, 0, sizeof(rgn.mTimeStamp));
	rgn.mTimeStamp.mFlags = kAudioTimeStampSampleTimeValid;
	rgn.mTimeStamp.mSampleTime = 0;
	rgn.mCompletionProc = NULL;
	rgn.mCompletionProcUserData = NULL;
	rgn.mAudioFile = audioFile;
	rgn.mLoopCount = 1;
	rgn.mStartFrame = 0;
	rgn.mFramesToPlay = UInt32(nPackets * fileFormat.mFramesPerPacket);
		
		// tell the file player AU to play all of the file
	XThrowIfError (au.SetProperty (kAudioUnitProperty_ScheduledFileRegion, 
			kAudioUnitScope_Global, 0,&rgn, sizeof(rgn)), "kAudioUnitProperty_ScheduledFileRegion");
	
		// prime the fp AU with default values
	UInt32 defaultVal = 0;
	XThrowIfError (au.SetProperty (kAudioUnitProperty_ScheduledFilePrime, 
			kAudioUnitScope_Global, 0, &defaultVal, sizeof(defaultVal)), "kAudioUnitProperty_ScheduledFilePrime");

		// tell the fp AU when to start playing (this ts is in the AU's render time stamps; -1 means next render cycle)
	AudioTimeStamp startTime;
	memset (&startTime, 0, sizeof(startTime));
	startTime.mFlags = kAudioTimeStampSampleTimeValid;
	startTime.mSampleTime = -1;
	XThrowIfError (au.SetProperty(kAudioUnitProperty_ScheduleStartTimeStamp, 
			kAudioUnitScope_Global, 0, &startTime, sizeof(startTime)), "kAudioUnitProperty_ScheduleStartTimeStamp");

	return fileDuration;
}



void MakeSimpleGraph (AUGraph &theGraph, CAAudioUnit &fileAU, CAStreamBasicDescription &fileFormat, AudioFileID audioFile)
{

	XThrowIfError (NewAUGraph (&theGraph), "NewAUGraph");
	// output node
	CAComponentDescription cd(kAudioUnitType_Output, kAudioUnitSubType_DefaultOutput, kAudioUnitManufacturer_Apple);


	AUNode outputNode;
	XThrowIfError (AUGraphAddNode (theGraph, &cd, &outputNode), "AUGraphAddNode");
	
	// file AU node
	AUNode fileNode;
	cd.componentType = kAudioUnitType_Generator;
	cd.componentSubType = kAudioUnitSubType_AudioFilePlayer;
	XThrowIfError (AUGraphAddNode (theGraph, &cd, &fileNode), "AUGraphAddNode");
    
    AUNode matrixMixerNode;
    cd.componentType = kAudioUnitType_Mixer;
    cd.componentSubType = kAudioUnitSubType_MatrixMixer;
    XThrowIfError (AUGraphAddNode (theGraph, &cd, &matrixMixerNode), "AUGraphAddNode");
    
    
	// connect & setup
	XThrowIfError (AUGraphOpen (theGraph), "AUGraphOpen");
	
	// install overload listener to detect when something is wrong
	AudioUnit anAU;
	XThrowIfError (AUGraphNodeInfo(theGraph, fileNode, NULL, &anAU), "AUGraphNodeInfo");
	
	fileAU = CAAudioUnit (fileNode, anAU);
    
    
    XThrowIfError (AUGraphNodeInfo(theGraph, matrixMixerNode, NULL, &matrixMixerAU), "AUGraphNodeInfo");
    matMixAU = CAAudioUnit(matrixMixerNode, matrixMixerAU);
    
    

// prepare the file AU for playback
// set its output channels
    XThrowIfError (fileAU.SetNumberChannels (kAudioUnitScope_Output, 0, fileFormat.NumberChannels()), "SetNumberChannels");

// set the output sample rate of the file AU to be the same as the file:
	XThrowIfError (fileAU.SetSampleRate (kAudioUnitScope_Output, 0, fileFormat.mSampleRate), "SetSampleRate");

// load in the file 
	XThrowIfError (fileAU.SetProperty(kAudioUnitProperty_ScheduledFileIDs, 
						kAudioUnitScope_Global, 0, &audioFile, sizeof(audioFile)), "SetScheduleFile");


	XThrowIfError (AUGraphConnectNodeInput (theGraph, fileNode,         0, matrixMixerNode, 0), "AUGraphConnectNodeInput");
    XThrowIfError (AUGraphConnectNodeInput (theGraph, matrixMixerNode,  0, outputNode,      0), "AUGraphConnectNodeInput");
    
    AudioStreamBasicDescription fileUnitFormat;
    XThrowIfError (fileAU.GetFormat(kAudioUnitScope_Output, 0, fileUnitFormat), "GetFormat");
    
    XThrowIfError (matMixAU.SetFormat(kAudioUnitScope_Input, 0, fileUnitFormat), "SetFormat");
    
    UInt32 inNumberChannels = fileUnitFormat.mChannelsPerFrame;
    printf("inNumberChannels: %d", inNumberChannels);
    
    AudioStreamBasicDescription outputUnitInputFormat;
    AudioUnit outputAu;
    XThrowIfError (AUGraphNodeInfo(theGraph, outputNode, NULL, &outputAu), "AUGraphNodeInfo");
    CAAudioUnit outAu = CAAudioUnit(outputNode, outputAu);
    
    XThrowIfError (outAu.GetFormat(kAudioUnitScope_Input, 0, outputUnitInputFormat), "GetFormat");
    
    UInt32 outNumberChannels = outputUnitInputFormat.mChannelsPerFrame;
    printf("outNumberChannels: %d", outNumberChannels);
    
    XThrowIfError (matMixAU.SetFormat(kAudioUnitScope_Output, 0, outputUnitInputFormat), "SetFormat");
    
    UInt32 matMixOutEleCount = inNumberChannels;
    UInt32 size = sizeof(matMixOutEleCount);
    
    // Get matrix mixer output bus count
    XThrowIfError (matMixAU.SetProperty(kAudioUnitProperty_ElementCount, kAudioUnitScope_Output, 0, &matMixOutEleCount, size), "SetProperty");
    
    printf("matMixOutEleCount:%d\n", matMixOutEleCount);
    
    UInt32 matMixInEleCount = outNumberChannels;
    size = sizeof(matMixInEleCount);
    // get matrix mixer input bus count
    XThrowIfError (matMixAU.SetProperty(kAudioUnitProperty_ElementCount, kAudioUnitScope_Input, 0, &matMixInEleCount, size), "SetProperty");
    printf("matMixInEleCount:%d\n", matMixInEleCount);
   
    

// AT this point we make sure we have the file player AU initialized
// this also propogates the output format of the AU to the output unit
	XThrowIfError (AUGraphInitialize (theGraph), "AUGraphInitialize");
	
    AudioUnitParameterValue value = 1.0f;
    UInt32 i, j;
    for (i = 0; i < matMixInEleCount; i++) {
        // Enable each input of the matrix mixer input channel
        XThrowIfError (matMixAU.SetParameter(kMatrixMixerParam_Enable, kAudioUnitScope_Input, i, value), "SetParameter");
    }
    
    for (i = 0; i < matMixOutEleCount; i++) {
        // printf("mixer o/p en ele:%d val:%f\n", i, value);
        
        // Enable each output of the matrix mixer input channel
        XThrowIfError (matMixAU.SetParameter(kMatrixMixerParam_Enable, kAudioUnitScope_Output, i, value), "SetParameter");
    }
    
    // TODO: inner 'j' loop can be removed later
    for (i = 0; i < matMixInEleCount; ++i) {
        for (j = 0; j < (matMixOutEleCount * outNumberChannels); ++j) {
            if (i == j) {
                UInt32 element = (i<<16) | j;
                //printf("mixer c/p en ele:%d val:%f\n", element, value);
                
                // set matrix mixer cross point gain
                XThrowIfError (matMixAU.SetParameter(kMatrixMixerParam_Volume, kAudioUnitScope_Global, element, value), "SetParameter");
            }
        }
    }
    
    for (i = 0; i <matMixInEleCount; ++i) {
        //printf("mixer i/p vol ele:%d val:%f\n", i, value);
        
        // set matrix mixer input element gain
        XThrowIfError (matMixAU.SetParameter(kMatrixMixerParam_Volume, kAudioUnitScope_Input, i, value), "SetParameter");
    }
    
    for (i = 0; i < matMixOutEleCount; ++i) {
        // printf("mixer o/p vol ele:%d val:%f\n", i, value);
        
        // set matrix mixer output element gain
        XThrowIfError (matMixAU.SetParameter(kMatrixMixerParam_Volume, kAudioUnitScope_Output, i, value), "SetParameter");
    }
    
    // set matrix mixer global element gain
    XThrowIfError (matMixAU.SetParameter(kMatrixMixerParam_Volume, kAudioUnitScope_Global, 0xFFFFFFFF, value), "SetParameter");
    
	// workaround a race condition in the file player AU
	usleep (10 * 1000);

// if we have a surround file, then we should try to tell the output AU what the order of the channels will be
	/*if (fileFormat.NumberChannels() > 2) {
		UInt32 layoutSize = 0;
		OSStatus err;
		XThrowIfError (err = AudioFileGetPropertyInfo (audioFile, kAudioFilePropertyChannelLayout, &layoutSize, NULL),
								"kAudioFilePropertyChannelLayout");
		
		if (!err && layoutSize) {
			char* layout = new char[layoutSize];
			
			err = AudioFileGetProperty(audioFile, kAudioFilePropertyChannelLayout, &layoutSize, layout);
			XThrowIfError (err, "Get Layout From AudioFile");

			
			err = AudioUnitSetProperty (outputAu, kAudioUnitProperty_AudioChannelLayout,
							kAudioUnitScope_Input, 0, layout, layoutSize);
			XThrowIfError (err, "kAudioUnitProperty_AudioChannelLayout");
			
			delete [] layout;
		}
	}*/
}